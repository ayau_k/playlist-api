<?php

namespace App\Repository;

use App\Entity\Track;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Track|null find($id, $lockMode = null, $lockVersion = null)
 * @method Track|null findOneBy(array $criteria, array $orderBy = null)
 * @method Track[]    findAll()
 * @method Track[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Track::class);
    }

    // /**
    //  * @return Track[] Returns an array of Track objects
    //  */

    public function selectYear()
    {
        return $this->createQueryBuilder('t')
            ->select('DISTINCT t.year')
            ->getQuery()
            ->getResult()
        ;
    }

    public function filtered($year, $author_id, $genre_id, array $sort)
    {
        $builder = $this->createQueryBuilder('t');

        if ($year)
            $builder = $builder->where('t.year=' . $year);

        if ($author_id)
            $builder = $builder->where('t.author_id=' . $author_id);

        if ($author_id)
            $builder = $builder->where('t.genre_id=' . $genre_id);

        array_walk($sort, function (&$item) {
           $item = 't.' . $item;
        });

        if ($sort)
            $builder->orderBy(implode(',', $sort), 'DESC');


        return $builder->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?Track
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
