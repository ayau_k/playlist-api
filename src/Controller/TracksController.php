<?php

namespace App\Controller;

use App\Entity\Author;
use App\Entity\Genre;
use App\Entity\Track;
use App\Repository\TrackRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TracksController extends AbstractController
{
    /**
     * @Route("/tracks", name="tracks")
     */
    public function index(Request $request): Response
    {
        $year = $request->get('year');
        $author = $request->get('author_');
        $genre = $request->get('genre');

        $sort = $request->get('sort', []);

        $list = $this->getDoctrine()
            ->getRepository(Track::class)
            ->filtered($year, $author, $genre, $sort);


        return $this->json([
            'data' => $list
        ], Response::HTTP_OK, ['Access-Control-Allow-Origin' => '*']);
    }

    /**
     * @Route("/genres", name="genres")
     */
    public function genres()
    {
        $list = $this->getDoctrine()
            ->getRepository(Genre::class)
            ->findAll();


        return $this->json([
            'data' => $list
        ], Response::HTTP_OK, ['Access-Control-Allow-Origin' => '*']);
    }

    /**
     * @Route("/authors", name="authors")
     */
    public function authors()
    {
        $list = $this->getDoctrine()
            ->getRepository(Author::class)
            ->findAll();


        return $this->json([
            'data' => $list
        ], Response::HTTP_OK, ['Access-Control-Allow-Origin' => '*']);
    }

    /**
     * @Route("/years", name="years")
     */
    public function years()
    {
        $list = $this->getDoctrine()
            ->getRepository(Track::class)
            ->selectYear();

        return $this->json([
            'data' => $list
        ], Response::HTTP_OK, ['Access-Control-Allow-Origin' => '*']);
    }
}
